<?php

/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * Map Form Values Behavior
 */
class openlayers_behavior_geofield extends openlayers_behavior {
  function options_init() {
    return array(
      'geofield' => '',
    );
  }

  function render(&$map) {
    $options = $this->options;
    $options['map_id'] = $map['id'];
    $options['wkt_source'] = $map['id'] . '-wkt';
    return $options;
  }
}
